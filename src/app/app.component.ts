import { Component } from "@angular/core";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css", "./app2.component.css"]
})
export class AppComponent {
  title = "hello2";
  message = 0;
  isDisabled = true;

  blink() {
    console.log("Hello World!");
    this.message = this.message + 1;
    this.isDisabled = false;
  }

  slashYou() {
    this.message = 2;
  }
}
